-- *********************************************************************
-- SQL to roll back currently unexecuted changes
-- *********************************************************************
-- Change Log: classpath:liquibase/db-changelog.xml
-- Ran at: 4/22/20 2:44 AM
-- Against: SA@jdbc:hsqldb:file:/config/db/airsonic
-- Liquibase version: 3.6.3
-- *********************************************************************

-- Lock Database
UPDATE DATABASECHANGELOGLOCK SET LOCKED = TRUE, LOCKEDBY = 'e6fb83a41b31 (172.20.0.4)', LOCKGRANTED = '2020-04-22 02:44:37.016' WHERE ID = 1 AND LOCKED = FALSE;

-- Release Database Lock
UPDATE DATABASECHANGELOGLOCK SET LOCKED = FALSE, LOCKEDBY = NULL, LOCKGRANTED = NULL WHERE ID = 1;

