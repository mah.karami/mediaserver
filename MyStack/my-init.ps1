# if your usb hdd (T:) is not available to share in docker desktop, enable windows sharing of the drive with appropriate user
# setup an A record for your Media Server in your DOMAINNAME to your local/router ip address.
# setup appropriate CNAME in you domaine to forward to your Media Server A record.
Set-ExecutionPolicy -ExecutionPolicy Unrestricted
docker network create traefik_proxy
docker-compose -f docker-compose-my1.yml up -d
# docker-compose -f docker-compose-my1.yml down